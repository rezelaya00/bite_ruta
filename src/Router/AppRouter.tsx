import {Routes, Route} from "react-router-dom";
import Menu from "../components/hola";
import Login from "../components/mundo";

export const AppRouter = () => {
    return (
        <Routes>
            <Route path="/login" element={<h1>Hola mundo</h1>}/>
            <Route path="/la" element={<Login/>}/>
            <Route path="/" element={<Menu/>}/>
            
            <Route path="/*" element={ <h1>Error!</h1> }/> {/* Si se coloca mal la ruta se puede redireccionar a otro lado */}
        </Routes>
    )
}

export default AppRouter
