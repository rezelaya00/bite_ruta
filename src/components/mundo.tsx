import "../App.css";

function Login () {
  return (
    <div>
      <h2>Login</h2>
      <form>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
          />
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
          />
        </div>
        <button type="submit">Log In</button>
      </form>
      <button><a href="/login">entrar</a></button>
    </div>
  );
}

export default Login;
